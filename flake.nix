{
  description = "YANR - Yet Another Nixpkg Ruby";

  inputs = {
    devshell.url = "github:numtide/devshell";
    devshell.inputs.nixpkgs.follows = "nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
    nix-filter.url = "github:numtide/nix-filter";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.11";
    nixpkgs-ruby.url = "github:nevesenin/nixpkgs-ruby/railsexpress";
    /* nixpkgs-ruby.url = "github:bobvanderlinden/nixpkgs-ruby"; */
    nixpkgs-ruby.inputs.nixpkgs.follows = "nixpkgs";
    bundix.url = "github:nevesenin/bundix/89d86c574f392fcf21d7bef89a48a4d5f568121e";
    bundix.flake = false;
  };

  outputs =
    { self
    , devshell
    , flake-utils
    , nix-filter
    , nixpkgs-ruby
    , nixpkgs
    , bundix
    }@inputs:
    let
      src = import ./modules/src.nix { inherit nix-filter; };

      yanrOverlays = import ./modules/yanr/overlays.nix { inherit inputs; };
    in
    rec {
      overlays.default = nixpkgs.lib.composeManyExtensions [
        nixpkgs-ruby.overlays.default
        yanrOverlays
        devshell.overlay
      ];
    } //
    flake-utils.lib.eachSystem [
      "aarch64-darwin"
      "x86_64-darwin"
      "x86_64-linux"
    ]
      (system:
      let
        yanrPkgs = import ./modules/pkgs.nix { inherit self inputs system; };
        yanrRuby = yanrPkgs."ruby-3.2.0";
      in
      rec {
        packages.default = yanrRuby;
        devShells = rec {
          default = dev;
          dev = yanrPkgs.devshell.mkShell {
            packages = [ yanrRuby yanrPkgs.nixpkgs-fmt ];
          };
        };
      }
      );
}
