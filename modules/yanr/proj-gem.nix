{ pkgs }:
{ gemName
, projGemset
}:
let
  depGems = builtins.map
    (depGemName:
      pkgs.buildProjGem { inherit projGemset; gemName = depGemName; }
    )
    projGemset."${gemName}".dependencies;

  attrs = projGemset."${gemName}" // {
    gemPath = pkgs.lib.unique (depGems ++ (pkgs.lib.flatten (builtins.map (g: g.gemPath) depGems)));
  };

  ret =
    if gemName == "sqlite3"
    then pkgs.lib.assertMsg false "${builtins.toJSON attrs}"
    else pkgs.buildRubyGem attrs;
in
pkgs.buildRubyGem attrs
