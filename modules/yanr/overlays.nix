{ inputs }: final: prev:

rec {
  defaultGemConfig = prev.defaultGemConfig // {
    psych = attrs:
      prev.lib.optionalAttrs (prev.lib.versionAtLeast attrs.version "5.0.0") {
        buildFlags = [ "--with-libyaml-dir=${final.libyaml}" ];
      };
  };

  buildRubyGem = args:
    (prev.buildRubyGem args).overrideAttrs (new: old:
      {
        nativeBuildInputs = [
          new.ruby
          final.makeWrapper
        ] ++ final.lib.optionals (new.type == "git") [ final.gitMinimal ]
        ++ old.nativeBuildInputs;
      }
    );

  buildProjGem = import ./proj-gem.nix { pkgs = final; };

  mkYanrShell = import ./proj-shell.nix {
    inherit inputs;
    pkgs = final;
  };
}
