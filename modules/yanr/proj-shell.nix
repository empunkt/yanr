{ inputs
,  pkgs
}:
{ projRoot
, projGemset ? null
, name ? null
, projRuby ? null
, projRubyVersion ? null
, useDevPreset ? false
, useMinimalEnv ? false
, gemEnvGroups ? [ "default" "test" "development" "production" ]
, services ? []
, projEnvVars ? []
}:
let
  projRubyVersion' =
    if projRubyVersion != null
    then projRubyVersion
    else
      pkgs.lib.strings.removePrefix "ruby-"
        (pkgs.lib.fileContents "${projRoot}/.ruby-version");
  presetGemset =
    if useDevPreset
    then import ./gem-presets/bundix.nix { inherit inputs; }
    else { };
  projGemset' =
    if projGemset != null
    then projGemset
    else
      if builtins.pathExists "${projRoot}/gemset.nix"
      then import "${projRoot}/gemset.nix"
      else { };
  projRuby' = if projRuby != null then projRuby else pkgs."ruby-${projRubyVersion'}";
  envName = if name != null then name else "yanr-ruby-${projRubyVersion'}-env";

  normalizedGemset = import ./proj-gemset.nix {
    inherit gemEnvGroups;
    lib = pkgs.lib;
    gemset = projGemset' // presetGemset;
    projRuby = projRuby';
    gemConfig = pkgs.defaultGemConfig;
  };

  projGems = map
    (gemName:
      pkgs.buildProjGem { inherit gemName; projGemset = normalizedGemset; }
    )
    (builtins.attrNames normalizedGemset);

  projEnv = pkgs.buildEnv {
    name = envName;
    paths = [ (pkgs.lib.lowPrio projRuby') ] ++ projGems;
    pathsToLink = [ "/" ];
  };

  projCommands = import ./proj-dev-deps { inherit pkgs services; };
in
pkgs.devshell.mkShell {
  packages = [ projEnv ];
  commands = projCommands;
  env = projEnvVars ++ [
    {
      name = "PRJ_YANR_DIR";
      eval = "$PRJ_ROOT/.yanr";
    }
    {
      name = "BUNDLE_GEMFILE";
      eval = "$PRJ_ROOT/Gemfile";
    }
    {
      name = "BUNDLE_PATH";
      unset = true;
    }
    {
      name = "GEM_HOME";
      value = "${projEnv}/${projRuby'.gemPath}";
    }
    {
      name = "GEM_PATH";
      value = "${projEnv}/${projRuby'.gemPath}";
    }
  ];
}
