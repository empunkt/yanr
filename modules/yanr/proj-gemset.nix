{ lib
, gemset
, projRuby
, gemEnvGroups ? [ "default" "test" "development" "production" ]
, gemConfig
}:

let
  groupFilter = name: attrs:
    let
      attrGroups = attrs.groups;
      envGroups = lib.unique (gemEnvGroups ++ [ "default" ]);
    in
    (lib.intersectLists envGroups attrGroups) != [ ];

  removeDepFromAttr = drvName: deps:
    builtins.filter
      (
        dep:
        (builtins.parseDrvName dep.name).name != drvName
      )
      deps;

  fixInput = attrs: inputName:
    lib.pipe attrs."${inputName}" [
      (removeDepFromAttr "bundler")
      (removeDepFromAttr "ruby")
      (removeDepFromAttr "rake")
    ];

  /* - Remove bundler and rake from inputs. Both should be present. It comes
       with rubygems, which is part of the ruby installation. If one needs a
       specific version, change it in rubygems or just add that version to the
       gemset (Gemfile).
     - Remove also all common known rubies from inputs since we want to use our
       own ruby */
  fixInputs = name: attrs:
    let
      fixedInputs = {
        buildInputs = fixInput attrs "buildInputs";
        nativeBuildInputs = fixInput attrs "nativeBuildInputs";
        propagatedBuildInputs = fixInput attrs "propagatedBuildInputs";
        propagatedUserEnvPkgs = fixInput attrs "nativeBuildInputs";
      };
    in
    attrs // fixedInputs;

  addProjRuby = name: attrs:
    let
      withProjRuby = {
        buildInputs = attrs.buildInputs ++ [ projRuby ];
        nativeBuildInputs = attrs.nativeBuildInputs ++ [ projRuby ];

        ruby = projRuby;
      };
    in
    attrs // withProjRuby;

  addGemConfig = gemName: attrs:
    let
      attrs' =
        if builtins.hasAttr gemName gemConfig
        then gemConfig."${gemName}" attrs
        else { };
    in
    attrs // attrs';

  setDefaults = gemName: attrs:
    let
      defaultAttrs = {
        gemName = gemName;
        groups =
          if (!(builtins.hasAttr "groups" attrs) || (attrs.groups == [ ]))
          then [ "default" ]
          else attrs.groups;
        buildInputs = attrs.buildInputs or [ ];
        nativeBuildInputs = attrs.nativeBuildInputs or [ ];
        propagatedBuildInputs = attrs.propagatedBuildInputs or [ ];
        propagatedUserEnvPkgs = attrs.propagatedUserEnvPkgs or [ ];
        dependencies = attrs.dependencies or [ ];
      };
      /* defaultAttrs = {}; */
    in
    attrs // defaultAttrs;

  normalizedGemset = lib.pipe gemset [
    (builtins.mapAttrs addGemConfig)
    (builtins.mapAttrs setDefaults)
    (lib.filterAttrs groupFilter)
    (builtins.mapAttrs fixInputs)
    (builtins.mapAttrs addProjRuby)
  ];
in
normalizedGemset
