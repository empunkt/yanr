{ pkgs, ... }:
let
  rabbitmqConfig = {
    config = pkgs.writeText "rabbitmq.conf" ''
      log.file = true
      log.console = false
    '';
    plugins = pkgs.writeText "plugins.conf" ''
      [rabbitmq_management,rabbitmq_management_agent].
    '';
  };

  setEnv = ''
    export RABBITMQ_HOME=$PRJ_YANR_DIR/rabbitmq
    export RABBITMQ_MNESIA_BASE=$RABBITMQ_HOME/mnesia
    export RABBITMQ_LOG_BASE=$RABBITMQ_HOME/log
    export RABBITMQ_CONFIG_FILE=${rabbitmqConfig.config}
    export RABBITMQ_ENABLED_PLUGINS_FILE=${rabbitmqConfig.plugins}
    export RABBITMQ_PID_FILE=$RABBITMQ_HOME/rabbitmq.pid
    export RABBITMQ_GENERATED_CONFIG_DIR=$RABBITMQ_HOME/config
  '';

  ensureRunning = ''
    echo "Waiting for RabbitMQ startup"
    ${pkgs.rabbitmq-server}/bin/rabbitmq-server -detached

    ${pkgs.rabbitmq-server}/bin/rabbitmqctl wait $RABBITMQ_PID_FILE
  '';

  ensureStopped = ''
    if [ -e "$RABBITMQ_PID_FILE" ]; then
      ${pkgs.rabbitmq-server}/bin/rabbitmqctl stop $RABBITMQ_PID_FILE
    fi
  '';

  ensureDirs = ''
    if [ ! -d "$RABBITMQ_HOME" ]; then
      mkdir -p $RABBITMQ_HOME -m 0777
      mkdir -p $RABBITMQ_MNESIA_BASE -m 0777
      mkdir -p $RABBITMQ_LOG_BASE -m 0777
      mkdir -p $RABBITMQ_GENERATED_CONFIG_DIR -m 0777
      echo "*" > $PRJ_YANR_DIR/.gitignore
    fi
  '';

  startCmd = pkgs.writeShellScriptBin "start-rabbitmq-server" ''
    set -euo pipefail
    ${setEnv}

    ${ensureDirs}

    ${ensureRunning}
  '';

  stopCmd = pkgs.writeShellScriptBin "stop-rabbitmq-server" ''
    set -euo pipefail
    ${setEnv}

    ${ensureStopped}
  '';

  resetCmd = pkgs.writeShellScriptBin "reset-rabbitmq-server" ''
    set -euo pipefail
    ${setEnv}

    ${ensureStopped}

    rm -rf $RABBITMQ_HOME

    ${ensureDirs}

    ${ensureRunning}
  '';

  statusCmd = pkgs.writeShellScriptBin "rabbitmq-server-status" ''
    set -euo pipefail
    ${setEnv}

    if [ -e "$RABBITMQ_PID_FILE" ]; then
      echo "RabbitMQ server is running"
    else
      echo "RabbitMQ server is stopped"
    fi
  '';
in
[
  {
    package = startCmd;
    help = ''
      Start a local RabbitMQ server.

      This creates a local RabbitMQ server instance running on port 5672.
      RabbitMQ management interface is running on port 15672.
      User/Password is guest/guest.
      ```
    '';
    category = "project service start commands";
  }
  {
    package = stopCmd;
    help = "Stop the local RabbitMQ server.";
    category = "project service stop commands";
  }
  {
    package = resetCmd;
    help = ''
      Reset the local RabbitMQ server.

      Purges `./yanr/rabbitmq` directory. This operation removes all RabbitMQ
      server data and initializes a completely new instance. In addition it
      starts the RabbitMQ server.
    '';
    category = "project service reset commands";
  }
  {
    package = statusCmd;
    help = "Status of the local RabbitMQ server.";
    category = "project service status commands";
  }
]
