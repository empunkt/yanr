{ pkgs
, services ? [] }:
let
  availableServices = [
    "memcached"
    "mysql"
    "postgresql"
    "rabbitmq"
    "redis"
  ];

  requestedServices = pkgs.lib.intersectLists services availableServices;

  commands = builtins.concatMap (elem:
    import (./. + "/${elem}.nix") { inherit pkgs; }
  ) requestedServices;
in
commands
