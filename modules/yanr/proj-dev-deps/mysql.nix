{ pkgs, ... }:
let
  setEnv = ''
    export DATA_DIR=$PRJ_YANR_DIR/mysql
    export MYSQL_SOCKET="$DATA_DIR/mysql.sock"
    export MYSQL_PORT="''${MYSQL_PORT:=3306}"
    export MYSQL_PID_FILE="$DATA_DIR/mysql.pid"
    export MYSQL_USER="''${MYSQL_USER:=yanr}"
    export MYSQL_PASSWORD="''${MYSQL_PASSWORD:=yanr}"
  '';

  ensureRunning = ''
    echo "Waiting for DB startup"
    ${pkgs.mysql80}/bin/mysqld \
      --datadir=$DATA_DIR \
      --socket=$MYSQL_SOCKET \
      --bind-address="127.0.0.1" \
      --port=$MYSQL_PORT \
      --pid-file=$MYSQL_PID_FILE &

    for i in {30..0}; do
      if ${pkgs.mysql80}/bin/mysql --socket=$MYSQL_SOCKET --user=root <<< 'SELECT 1' &> /dev/null; then
        break
      fi
      sleep 1
    done
    if [ "$i" = 0 ]; then
      echo "Unable to start server."
      exit
    fi
  '';

  ensureInit = ''
    if [ ! -d "$DATA_DIR" ]; then
      mkdir -p $DATA_DIR
      echo "*" > $PRJ_YANR_DIR/.gitignore

      ${initMysql}

      ${ensureRunning}

      echo "Creating user"
      ${pkgs.mysql80}/bin/mysql --socket=$MYSQL_SOCKET --user=root <<< "CREATE USER '$MYSQL_USER'@'%' IDENTIFIED BY '$MYSQL_PASSWORD';"

      ${pkgs.mysql80}/bin/mysql --socket=$MYSQL_SOCKET --user=root <<< "GRANT ALL ON *.* TO '$MYSQL_USER'@'%';"

      ${pkgs.mysql80}/bin/mysql --socket=$MYSQL_SOCKET --user=root <<< "FLUSH PRIVILEGES;"

      ${ensureStopped}
    fi
  '';

  ensureStopped = ''
    if [ -e "$MYSQL_SOCKET" ]; then
      ${pkgs.mysql80}/bin/mysqladmin --socket=$MYSQL_SOCKET --user=root shutdown
    fi
  '';

  initMysql = ''
    ${pkgs.mysql80}/bin/mysqld \
      --user="$MYSQL_USER" \
      --datadir=$DATA_DIR \
      --initialize-insecure
  '';

  startCmd = pkgs.writeShellScriptBin "start-mysql-server" ''
    set -euo pipefail
    ${setEnv}

    ${ensureInit}

    ${ensureRunning}
  '';

  stopCmd = pkgs.writeShellScriptBin "stop-mysql-server" ''
    set -euo pipefail
    ${setEnv}

    ${ensureStopped}
  '';

  resetCmd = pkgs.writeShellScriptBin "reset-mysql-server" ''
    set -euo pipefail
    ${setEnv}

    ${ensureStopped}

    rm -rf $DATA_DIR

    ${ensureInit}

    ${ensureRunning}
  '';

  statusCmd = pkgs.writeShellScriptBin "mysql-server-status" ''
    set -euo pipefail
    ${setEnv}

    if [ -e "$MYSQL_SOCKET" ]; then
      echo "MySQL server is running"
    else
      echo "MySQL server is stopped"
    fi
  '';
in
[
  {
    package = startCmd;
    help = ''
      Start a local MySQL server.

      This creates a local MySQL server instance running on port 3306 (default).
      There is a user `yanr` with password `yanr`. If you want the server to run
      on a different port and/or different user/password, please set the
      `projEnvVars` argument for `mkYanrShell` accordingly:
      ```
        projEnvVars = [
          {
            name = "MYSQL_PORT";
            value = 123456;
          }
          {
            name = "MYSQL_USER";
            value = "foo";
          }
          {
            name = "MYSQL_PASSWORD";
            value = "bar";
          }
        ];
      ```
    '';
    category = "project service start commands";
  }
  {
    package = stopCmd;
    help = "Stop the local MySQL server.";
    category = "project service stop commands";
  }
  {
    package = resetCmd;
    help = ''
      Reset the local MySQL server.

      Purges `./yanr/mysql` directory. This operation removes all MySQL server
      data and initializes a completely new instance. In addition it starts the
      MySQL server.
    '';
    category = "project service reset commands";
  }
  {
    package = statusCmd;
    help = "Status of the local MySQL server.";
    category = "project service status commands";
  }
]
