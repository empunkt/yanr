{ inputs }:

{
  bundix = {
    src = inputs.bundix;
    version = "2.5.1-p1";
  };
}
