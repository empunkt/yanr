{ nix-filter }:
let
  _nixFilterSrc = nix-filter {
    root = ../.;
    include = [
      ../.gitignore
      ../flake.lock
      ../flake.nix
      ../modules/pkgs.nix
      ../modules/src.nix
      ../modules/yanr/gem-presets/development.nix
      ../modules/yanr/overlays.nix
      ../modules/yanr/proj-gem.nix
      ../modules/yanr/proj-gemset.nix
      ../template/flake.nix
    ];
  };
in
_nixFilterSrc
