{ self, inputs, system }:
let
  yanrPkgs = import inputs.nixpkgs {
    inherit system;
    overlays = [ self.overlays.default ];
  };
in

yanrPkgs
